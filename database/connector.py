import traceback

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import sessionmaker


def engine_maker(database_port="8000"):

    sql_url = "mysql+pymysql://root:0123456789@localhost:{port}/demo".format(
        port=database_port
    )

    # get engine
    try:
        engine = create_engine(sql_url)
    except Exception:
        engine = None
        print(traceback.print_exc())

    return engine


def session_maker(engine=None):

    # get engine
    if not engine:
        engine = engine_maker()
        if not engine:
            return None

    # get session
    session = sessionmaker(bind=engine)
    return session()


def get_table(table_name="", engine=None):
    # get engine
    if not engine:
        engine = engine_maker()
        if not engine:
            return None

    # get table
    metadata = MetaData()
    try:
        table = Table(table_name, metadata, autoload_with=engine)
    except Exception:
        table = None
        print(traceback.print_exc())

    return table
