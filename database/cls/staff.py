import os
import sys

CURRENT_DIR = os.path.dirname(__file__)
DATABASE_DIR = os.path.dirname(__file__)

if not DATABASE_DIR in sys.path:
    sys.path.append(DATABASE_DIR)

import task as task_class
from table import task as task_table
from table import staff as staff_table


class Staff:
    def __init__(self, name="", surname="", role=""):
        # init class var
        self.id = "000000"
        self.name = name
        self.surname = surname
        self.role = ""
        self.is_admin = False
        self.active = False
        self.task_dict = {}
        # query data from database
        self.update_staff_info()

    def update_staff_info(self):
        # empty task dict
        self.task_dict = {}

        # query data from database
        staff_record = staff_table.get_staff_record(
            name=self.name, surname=self.surname
        )
        if not staff_record:
            self.active = False
            return

        # update class vars
        self.id = staff_record.id
        self.name = staff_record.name
        self.surname = staff_record.surname
        self.role = staff_record.role
        self.is_admin = staff_record.admin
        self.active = staff_record.active

        # get all task assign to staff
        task_id_query = task_table.get_assignee_tasks_id(staff_id=self.id)
        # insert value to dict
        for task_id in task_id_query:
            self.task_dict[task_id.id] = task_class.Task(task_id=task_id.id)

    def register_staff(self):
        if self.active:
            print("Already register staff")
            return

        # insert record to database
        staff_table.insert_staff_record(
            name=self.name,
            surname=self.surname,
            role=self.role,
        )
        # query new data from database
        self.update_staff_info()

    def deactive_staff(self):
        if self.active:
            # update record data in database
            staff_table.update_staff_info(staff_id=self.id, active=False)
            # query new data from database
            self.update_staff_info()

    def active_staff(self):
        if self.active:
            # update record data in database
            staff_table.update_staff_info(staff_id=self.id, active=True)
            # query new data from database
            self.update_staff_info()

    def change_information(self, name="", surname="", role=""):
        if (name or surname or role) and self.active:
            update_name = name if name else self.name
            update_surname = surname if surname else self.surname
            update_role = role if role else self.role
            # update record data in database
            staff_table.update_staff_info(
                staff_id=self.id,
                name=update_name,
                surname=update_surname,
                role=update_role,
                role=role,
            )
            # query new data from database
            self.update_staff_info()
