import os
import sys
from datetime import datetime

CURRENT_DIR = os.path.dirname(__file__)
DATABASE_DIR = os.path.dirname(__file__)

if not DATABASE_DIR in sys.path:
    sys.path.append(DATABASE_DIR)

from table import task


class Task:

    def __init__(self, task_id="000000"):
        # init class var
        self.task_id = task_id
        self.name = ""
        self.assignee_id = ""
        self.start_date = None
        self.due_date = None
        self.status = ""
        self.active = False
        # query data from database
        self.update_task_info()

    def update_task_info(self):
        task_query = task.get_task_record(task_id=self.task_id)
        if not task_query:
            self.active = False
            return

        # assign new data
        self.name = task_query.name
        self.assignee_id = task_query.assignee_id
        self.start_date = task_query.due_date
        self.status = task_query.status
        self.active = True

    def register_task(self, name="", due_date=None):
        if not self.active and name and due_date:
            # insert database reocrd
            task.insert_task_record(
                name=name,
                start_date=datetime.now(),
                due_date=due_date,
            )
            # query new data from database
            self.update_task_info()

    def change_information(
        self,
        name="",
        assignee_id="",
        due_date=None,
        status="wait to start",
    ):
        if self.active():
            # update data
            update_name = name if name else self.name
            update_due_date = due_date if due_date else self.due_date
            update_status = stat if status else self.status
            update_assignee = assignee_id if assignee_id else self.assignee_id

            # update database record
            task.update_task_record(
                task_id=self.task_id,
                name=update_name,
                due_date=update_due_date,
                status=update_status,
                assignee_id=update_assignee,
            )

            # query new data from database
            self.update_task_info()
