import os
import sys

CURRENT_DIR = os.path.dirname(__file__)

if not CURRENT_DIR in sys.path:
    sys.path.append(CURRENT_DIR)
