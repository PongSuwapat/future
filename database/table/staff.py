import os
import sys
import traceback

from sqlalchemy import and_, Update

CURRENT_DIR = os.path.dirname(__file__)
DATABASE_DIR = os.path.dirname(CURRENT_DIR)

if not DATABASE_DIR in sys.path:
    sys.path.append(DATABASE_DIR)

import connector
import task


def get_staff_table(engine=None):
    return connector.get_table(table_name="staff", engine=engine)


def get_staff_record(name="", surname="", engine=None):
    # get table
    staff_table = get_staff_table(engine=engine)
    session = connector.session_maker(engine=engine)

    # must have name surname and session
    if not (name or surname or session):
        return

    # query data
    staff_query = (
        session.query(staff_table)
        .filter(
            and_(
                staff_table.columns.name == name,
                staff_table.columns.surname == surname,
            )
        )
        .one_or_none()
    )
    session.close()

    return staff_query


def get_staff_from_id(staff_id="000000", engine=None):
    # get table & session
    staff_table = get_staff_table(engine=engine)
    session = connector.session_maker(engine=engine)

    #  incorrect staff id or can't make session
    if not (name or surname or session):
        return

    # query data
    staff_query = (
        session.query(staff_table)
        .filter(
            staff_table.columns.id == staff_id,
        )
        .one_or_none()
    )
    session.close()

    return staff_query


def update_staff_record(
    staff_id="000000",
    name="",
    surname="",
    role="",
    admin=False,
    active=False,
    engine=None,
):
    # get engine
    if not engine:
        engine = connector.engine_maker()

    if (not staff_id) or (staff_id == "000000") or (engine):
        return

    # get original record
    original_record = get_staff_from_id(staff_id=staff_id, engine=engine)
    if not original_record:
        print("Record not exists in database")
        return

    # get table and session
    staff_table = get_staff_table(engine=engine)
    session = connector.session_maker()

    # data to update
    update_name = original_record.name if name == original_record.name else name
    update_surname = original_record.surname if surname == original_record else surname
    update_role = original_record.role if role == original_record.role else role
    update_active = (
        original_record.active if active == original_record.active else active
    )
    update_admin = original_record.admin if admin == original_record.admin else admin

    # update statment to database
    try:
        # update statement
        statement = (
            Update(staff_table)
            .where(staff_table.columns.id == original_record.id)
            .values(
                name=update_name,
                surname=update_surname,
                role=update_role,
                active=update_active,
                admin=update_admin,
            )
        )
        session.execute(statement)
        session.commit()
        print("Update record success")
    except:
        print(traceback.print_exc())
    finally:
        session.close()


def insert_staff_record(name="", surname="", role="", engine=None):

    # get engine
    if not engine:
        engine = connector.engine_maker()

    # check empty value
    if not ((name or surname or role) and engine):
        return

    # get table and session
    staff_table = get_staff_table(engine=engine)
    session = connector.session_maker(engine=engine)

    # get lastest inserted staff id
    try:
        query_id = (
            session.query(staff_table.columns.id)
            .order_by(staff_table.columns.id.desc())
            .first()
        )
        if not query_id:
            latest_id = "000000"
        else:
            latest_id = query_id.id

    except Exception:
        print(traceback.print_exc())
        latest_id = "000000"

    # create insert dict
    insert_dict = {
        "id": str(int(latest_id) + 1).zfill(6),
        "name": name,
        "surname": surname,
        "role": role,
        "admin": False,
        "active": True,
    }

    # insert to database
    try:
        staff_record = staff_table.insert().values(insert_dict)
        session.execute(staff_record)
        session.commit()
        print("Insert Success")
    except Exception:
        print(traceback.print_exc())
    finally:
        session.close()
