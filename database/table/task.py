import os
import sys
import traceback

from sqlalchemy import and_, Update

CURRENT_DIR = os.path.dirname(__file__)
DATABASE_DIR = os.path.dirname(CURRENT_DIR)

if not DATABASE_DIR in sys.path:
    sys.path.append(DATABASE_DIR)

import connector


def get_task_table(engine=None):
    return connector.get_table(table_name="task", engine=engine)


def get_assignee_tasks_id(staff_id="000000", engine=None):
    # get table
    task_table = get_task_table(engine=engine)
    session = connector.session_maker(engine=engine)

    #  incorrect staff id or can't make session
    if (not staff_id) or (staff_id == "000000") or (not session):
        return

    # query data
    task_query = session.query(task_table.columns.task_id).where(
        task_table.columns.assignee_id == staff_id,
    )
    task_record = task_query.all()
    session.close()

    return task_record


def get_task_record(task_id="000000", engine=None):
    # get table
    task_table = get_task_table(engine=engine)
    session = connector.session_maker(engine=engine)

    #  incorrect task_id or can't make session
    if (not task_id) or (task_id == "000000") or (not session):
        return

    # query data
    task_query = (
        session.query(task_table)
        .filter(
            task_table.columns.task_id == task_id,
        )
        .one_or_none()
    )
    session.close()

    return task_query


def update_task_record(
    task_id="000000",
    name="",
    assignee_id="000000",
    start_date=None,
    due_date=None,
    status="wait to start",
    engine=None,
):
    # get engine
    if not engine:
        engine = connector.engine_maker()

    if (not task_id) or (task_id == "000000") or (not engine):
        return

    # get original record
    original_record = get_task_record(task_id=task_id, engine=engine)
    if not original_record:
        print("Record not exists in database")
        return

    # get table and session
    task_table = get_task_table(engine=engine)
    session = connector.session_maker()

    # data to update
    update_name = original_record.name if (not name) else name
    update_assignee = (
        original_record.assignee_id
        if (not assignee_id) or (assignee_id == "000000")
        else assignee_id
    )
    update_start_date = original_record.start_date if (not start_date) else start_date
    update_due_date = original_record.due_date if (not due_date) else due_date
    update_status = original_record.status if (not status) else status

    # update statment to database
    try:
        statement = (
            Update(task_table)
            .where(task_table.columns.task_id == task_id)
            .values(
                name=update_name,
                assignee_id=update_assignee,
                start_date=update_start_date,
                due_date=update_due_date,
                status=update_status,
            )
        )
        session.execute(statement)
        session.commit()
        print("Update record success")
    except:
        print(traceback.print_exc())
    finally:
        session.close()


def insert_task_record(
    name="",
    assignee_id="000000",
    start_date=None,
    due_date=None,
    engine=None,
):

    # get engine
    if not engine:
        engine = connector.engine_maker()

    # check empty value
    if not ((name or surname or role) and engine):
        return

    # get table and session
    task_table = get_task_table(engine=engine)
    session = connector.session_maker(engine=engine)

    # get lastest staff insert
    try:
        query_id = (
            session.query(task_table.columns.task_id)
            .order_by(task_table.columns.task_id.desc())
            .first()
        )
        if not query_id:
            latest_id = "000000"
        else:
            latest_id = query_id.task_id

    except Exception:
        print(traceback.print_exc())
        latest_id = "000000"

    # create insert dict
    insert_dict = {
        "task_id": str(int(latest_id) + 1).zfill(6),
        "name": name,
        "assignee_id": assignee_id,
        "start_date": start_date,
        "due_date": due_date,
        "status": "wait to start",
    }

    # insert to database
    try:
        tasK_record = task_table.insert().values(insert_dict)
        session.execute(tasK_record)
        session.commit()
        print("Insert Success")
    except Exception:
        print(traceback.print_exc())
    finally:
        session.close()
